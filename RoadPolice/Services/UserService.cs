﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RoadPolice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoadPolice.Services
{
    public class UserService
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;

        public UserService()
        {
            var context = new ApplicationDbContext();
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        }
        public bool IsUserAdmin(string userName)
        {
            var user = userManager.FindByName(userName);
            if (user == null)
            {
                return false;
            }
            return userManager.IsInRole(user.Id, "admin"); 
        }
        public bool IsUserDriver(string userName)
        {
            var user = userManager.FindByName(userName);
            if (user == null)
            {
                return false;
            }
            return userManager.IsInRole(user.Id, "driver");
        }
        public bool IsUserInspector(string userName)
        {
            var user = userManager.FindByName(userName);
            if (user == null)
            {
                return false;
            }
            return userManager.IsInRole(user.Id, "inspector");
        }
    }
}