﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class PoliceProtocol
    {

        public PoliceProtocol()
        {
            IsPaid = false;
        }

        public int Id { get; set; }

        public DateTime FineDate { get; set; }
        [Required(ErrorMessage = "Поле \"Водитель\" должно быть заполнено")]
        public int DriverId { get; set; }
        public virtual Driver Driver { get; set; }

        [Required(ErrorMessage = "Поле \"Автомобиль\" должно быть заполнено")]
        public string Vehicle { get; set; }

        [Required(ErrorMessage = "Поле \"Гос. номер\" должно быть заполнено")]
        public string VehicleNumber { get; set; }
        [Required(ErrorMessage = "Поле \"Инспектор\" должно быть заполнено")]
        public int PoliceInspectorId { get; set; }
        [NotMapped]
        public string PoliceFullName { get; set; }
        public virtual PoliceInspector PoliceInspector { get; set; }
        [Required(ErrorMessage = "Поле \"Штраф\" должно быть заполнено")]
        public int FineId { get; set; }
        public virtual Fine Fine { get; set; }

        [Required(ErrorMessage = "Поле \"Стоимость\" должно быть заполнено")]
        public int Cost { get; set; }

        [Required(ErrorMessage = "Поле \"Место составления протокола\" должно быть заполнено")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Поле \"Доп. информация\" должно быть заполнено")]
        public string Description { get; set; }

        public bool IsPaid { get; set; }

        public DateTime PaidDate { get; set; }

    }
}