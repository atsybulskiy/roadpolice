﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class ProtocolStatistic
    {
        public string Title { get; set; }

        public int Count { get; set; }
    }
}