﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class Fine
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Название")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Стоимость")]
        public int Price { get; set; }
    }
}