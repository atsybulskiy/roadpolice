﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class MenuViewModel
    {
        public bool IsAdmin { get; set; }
        public bool IsDriver { get; set; }
        public bool IsInspector { get; set; }
    }
}