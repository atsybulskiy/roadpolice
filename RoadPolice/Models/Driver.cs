﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class Driver
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле \"ФИО\" должно быть заполнено")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Поле \"Место работы\" должно быть заполнено")]
        public string Workplace { get; set; }

        [Required(ErrorMessage = "Поле \"Адрес\" должно быть заполнено")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Поле \"Номер водительского удостоверения\" должно быть заполнено")]
        public string LicenseNumber { get; set; }
    }
}