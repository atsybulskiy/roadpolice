﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RoadPolice.Models
{
    public class PoliceInspector
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле \"ФИО\" необходимо заполнить")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Поле \"Звание\" необходимо заполнить")]
        public string Rank { get; set; }

        [Required(ErrorMessage = "Поле \"Email\" должно быть заполнено")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [NotMapped]
        [Required]
        [StringLength(100, ErrorMessage = "Длина поля {0} должна быть больше {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }
}