namespace RoadPolice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePoliceProticolModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PoliceProtocols", "FineId", c => c.Int(nullable: false));
            CreateIndex("dbo.PoliceProtocols", "FineId");
            AddForeignKey("dbo.PoliceProtocols", "FineId", "dbo.Fines", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PoliceProtocols", "FineId", "dbo.Fines");
            DropIndex("dbo.PoliceProtocols", new[] { "FineId" });
            DropColumn("dbo.PoliceProtocols", "FineId");
        }
    }
}
