namespace RoadPolice.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RoadPolice.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(RoadPolice.Models.ApplicationDbContext context)
        {
            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var roleAdmin = "admin";
            var roleDriver = "driver";
            var roleInspector = "inspector";

            if (!roleManager.RoleExists(roleAdmin))
            {
                 roleManager.Create(new IdentityRole(roleAdmin));
            }
            if (!roleManager.RoleExists(roleDriver))
            {
                roleManager.Create(new IdentityRole(roleDriver));
            }
            if (!roleManager.RoleExists(roleInspector))
            {
                roleManager.Create(new IdentityRole(roleInspector));
            }
            context.SaveChanges();
            var user = new ApplicationUser { UserName = "test@gmail.com", Email = "test@gmail.com" };
            var result = userManager.Create(user, "Qwerty12345#");

            if (result.Succeeded)
            {
                userManager.AddToRole(user.Id, roleAdmin);
            }
            context.SaveChanges();

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
