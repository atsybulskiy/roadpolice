namespace RoadPolice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePriceTypeField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PoliceInspectors", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Fines", "Price", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Fines", "Price", c => c.String(nullable: false));
            DropColumn("dbo.PoliceInspectors", "Email");
        }
    }
}
