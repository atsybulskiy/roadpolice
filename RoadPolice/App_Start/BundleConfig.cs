﻿using System.Web;
using System.Web.Optimization;

namespace RoadPolice
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));
            
            bundles.Add(new StyleBundle("~/Content/theme").Include(
                    "~/Content/plugins/bootstrap/css/bootstrap.css",
                    "~/Content/plugins/bootstrap-select/css/bootstrap-select.css",
                    "~/Content/plugins/node-waves/waves.css", 
                    "~/Content/plugins/animate-css/animate.css",
                    "~/Content/css/materialize.css",
                    "~/Content/css/style.css",
                    "~/Content/css/themes/all-themes.css"));
        }
    }
}
