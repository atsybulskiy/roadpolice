﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RoadPolice.Startup))]
namespace RoadPolice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
