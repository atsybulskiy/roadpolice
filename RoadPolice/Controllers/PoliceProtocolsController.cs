﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RoadPolice.Models;
using RoadPolice.Services;

namespace RoadPolice.Controllers
{
    [Authorize]
    public class PoliceProtocolsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PoliceProtocols
        public ActionResult Index(string sortOrder)
        {
            //var policeProtocols = db.PoliceProtocols.Include(p => p.Driver).Include(p => p.PoliceInspector);
            //return View(policeProtocols.ToList());



            //ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            var protocols = from s in db.PoliceProtocols.Include(p => p.Driver).Include(p => p.PoliceInspector)
                           select s;
            switch (sortOrder)
            {
                case "driver_desc":
                    protocols = protocols.OrderByDescending(s => s.Driver.FullName);
                    break;
                case "driver":
                    protocols = protocols.OrderBy(s => s.Driver.FullName);
                    break;

                case "inspector_desc":
                    protocols = protocols.OrderByDescending(s => s.PoliceInspector.FullName);
                    break;
                case "inspector":
                    protocols = protocols.OrderBy(s => s.PoliceInspector.FullName);
                    break;

                case "date_desc":
                    protocols = protocols.OrderBy(s => s.FineDate);
                    break;
                case "date":
                    protocols = protocols.OrderByDescending(s => s.FineDate);
                    break;

                case "price":
                    protocols = protocols.OrderBy(s => s.Cost);
                    break;
                case "price_desc":
                    protocols = protocols.OrderByDescending(s => s.Cost);
                    break;

                case "pay_desc":
                    protocols = protocols.OrderBy(s => s.IsPaid);
                    break;
                case "pay":
                    protocols = protocols.OrderByDescending(s => s.IsPaid);
                    break;

                default:
                    protocols = protocols.OrderByDescending(s => s.Id);
                    break;
            }

            return View(protocols.ToList());

        }

        // GET: PoliceProtocols/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceProtocol policeProtocol = db.PoliceProtocols.Find(id);
            if (policeProtocol == null)
            {
                return HttpNotFound();
            }
            return View(policeProtocol);
        }

        // GET: PoliceProtocols/Create
        public ActionResult Create()
        {
            var userService = new UserService();
            var model = new PoliceProtocol();
            var isAdmin = userService.IsUserAdmin(User.Identity.Name);
            ViewBag.IsAdmin = isAdmin;
            if (isAdmin)
            {
                ViewBag.PoliceInspectorId =
                    new SelectList(db.PoliceInspectors.OrderBy(x => x.FullName),
                        "Id", "FullName");
            }
            else
            {
                var policeman = db.PoliceInspectors.FirstOrDefault(x => x.Email == User.Identity.Name);
                if (policeman != null)
                {
                    ViewBag.PoliceFullName = policeman.FullName;
                    model.PoliceInspectorId = policeman.Id;
                }
            }
            ViewBag.DriverId = new SelectList(db.Drivers.OrderBy(x => x.FullName), "Id", "FullName");
            ViewBag.FineId = new SelectList(db.Fines, "Id", "Title");
            return View(model);
        }

        // POST: PoliceProtocols/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DriverId,PoliceInspectorId,Vehicle,VehicleNumber,FineId,Cost,Address,Description,IsPaid,PaidDate")] PoliceProtocol policeProtocol)
        {
            if (ModelState.IsValid)
            {

                DateTime currentDate = DateTime.Now;

                policeProtocol.FineDate = currentDate;

                if (policeProtocol.IsPaid)
                {
                    policeProtocol.PaidDate = currentDate;
                }

                db.PoliceProtocols.Add(policeProtocol);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "FullName", policeProtocol.DriverId);
            ViewBag.FineId = new SelectList(db.Fines, "Id", "Title", policeProtocol.FineId);
            ViewBag.PoliceInspectorId = new SelectList(db.PoliceInspectors, "Id", "FullName", policeProtocol.PoliceInspectorId);
            return View(policeProtocol);
        }

        // GET: PoliceProtocols/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceProtocol policeProtocol = db.PoliceProtocols.Find(id);
            if (policeProtocol == null)
            {
                return HttpNotFound();
            }
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "FullName", policeProtocol.DriverId);
            ViewBag.FineId = new SelectList(db.Fines, "Id", "Title", policeProtocol.FineId);
            ViewBag.PoliceInspectorId = new SelectList(db.PoliceInspectors, "Id", "FullName", policeProtocol.PoliceInspectorId);
            return View(policeProtocol);
        }

        // POST: PoliceProtocols/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FineDate,DriverId,PoliceInspectorId,FineId,Cost,Address,Description,IsPaid,PaidDate")] PoliceProtocol policeProtocol)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policeProtocol).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DriverId = new SelectList(db.Drivers, "Id", "FullName", policeProtocol.DriverId);
            ViewBag.FineId = new SelectList(db.Fines, "Id", "Title", policeProtocol.FineId);
            ViewBag.PoliceInspectorId = new SelectList(db.PoliceInspectors, "Id", "FullName", policeProtocol.PoliceInspectorId);
            return View(policeProtocol);
        }

        // GET: PoliceProtocols/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceProtocol policeProtocol = db.PoliceProtocols.Find(id);
            if (policeProtocol == null)
            {
                return HttpNotFound();
            }
            return View(policeProtocol);
        }

        // POST: PoliceProtocols/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PoliceProtocol policeProtocol = db.PoliceProtocols.Find(id);
            db.PoliceProtocols.Remove(policeProtocol);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
