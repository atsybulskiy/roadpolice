﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using RoadPolice.Models;

namespace RoadPolice.Controllers
{
    public class ProtocolsStatisticController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProtocolsStatistic
        public ActionResult Index(string type, string startDate, string endDate)
        {

            DateTime baselineDate = getBaseDate(type);

            ViewBag.DateType = type;


            //            var arr =  db.PoliceProtocols.Where(a => a.Id > 0)
            //                         .GroupBy(a => a.Fine)
            //                         .Select(x => new { x.Key,
            //                             x.Key.Title,
            //                             Count = x.Count() });

            var protocolStatistics = db.PoliceProtocols.Where(a => a.FineDate > baselineDate)
                         .GroupBy(a => a.Fine)
                         .Select(x => new ProtocolStatistic
                         {
                             Title = x.Key.Title,
                             Count = x.Count()
                         });


            if (startDate != null && endDate != null)
            {

                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);

                ViewBag.sDate = startDate;
                ViewBag.eDate = endDate;

               protocolStatistics = db.PoliceProtocols.Where(a => a.FineDate >= sDate && a.FineDate <= eDate)
                        .GroupBy(a => a.Fine)
                        .Select(x => new ProtocolStatistic
                        {
                            Title = x.Key.Title,
                            Count = x.Count()
                        });

            }

            return View(protocolStatistics.ToList());
        }

        public ActionResult GetChart(string type, string startDate, string endDate, int chartTypeCode)
        {

            var baseDate = getBaseDate(type);

            string chartType = chartTypeCode == 0 ? "column" : "pie";

            var protocolStatistics = db.PoliceProtocols.Where(a => a.FineDate > baseDate)
                       .GroupBy(a => a.Fine)
                       .Select(x => new ProtocolStatistic
                       {
                           Title = x.Key.Title,
                           Count = x.Count()
                       });

            if (startDate != null && endDate != null)
            {

                var sDate = Convert.ToDateTime(startDate);
                var eDate = Convert.ToDateTime(endDate);


                protocolStatistics = db.PoliceProtocols.Where(a => a.FineDate >= sDate && a.FineDate <= eDate)
                         .GroupBy(a => a.Fine)
                         .Select(x => new ProtocolStatistic
                         {
                             Title = x.Key.Title,
                             Count = x.Count()
                         });

            }

            var xValues = new ArrayList();
            var yValues = new ArrayList();

            foreach (var item in protocolStatistics)
            {
                xValues.Add(item.Title);
                yValues.Add(item.Count);
            }


//            new Chart(width: 800, height: 500, theme: ChartTheme.Blue)
            new Chart(width: 800, height: 500)
                .AddTitle("Статистика правонарушений:")
                .AddLegend()
                .AddSeries("default", chartType: chartType, xValue: xValues, yValues: yValues)
                .Write("bmp");

            return null;
        }


        private DateTime getBaseDate(string type)
        {
            DateTime baselineDate;



            switch (type)
            {
                case "week":
                    baselineDate = DateTime.Today.AddDays(-7);
                    break;
                case "month":
                    baselineDate = DateTime.Today.AddMonths(-1);
                    break;

                case "year":
                    baselineDate = DateTime.Today.AddMonths(-12);
                    break;

                default:
                    baselineDate = DateTime.Today.AddMonths(-999);
                    break;
            }

            return baselineDate;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
