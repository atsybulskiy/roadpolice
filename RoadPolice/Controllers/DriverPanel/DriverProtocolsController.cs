﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RoadPolice.Models;

namespace RoadPolice.Controllers.DriverPanel
{
    public class DriverProtocolsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DriverProtocols
        public ActionResult Index()
        {
            var policeProtocols = db.PoliceProtocols.Include(p => p.Driver).Include(p => p.Fine).Include(p => p.PoliceInspector);
            return View(policeProtocols.ToList());
        }

        // GET: DriverProtocols/Details/5
        public ActionResult Details(string id)
        {
            var policeProtocols = db.PoliceProtocols.Include(p => p.Driver).Include(p => p.Fine).Include(p => p.PoliceInspector).Where(x => x.Driver.LicenseNumber == id);
            return View(policeProtocols.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
