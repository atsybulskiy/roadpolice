﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RoadPolice.Models;
using RoadPolice.Services;

namespace RoadPolice.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Menu()
        {
            var service = new UserService();
            var model = new MenuViewModel();
            if (!User.Identity.IsAuthenticated)
                return PartialView(model);
            model.IsAdmin = service.IsUserAdmin(User.Identity.Name);
            model.IsDriver = service.IsUserDriver(User.Identity.Name);
            model.IsInspector = service.IsUserInspector(User.Identity.Name);
            return PartialView(model);
        }

    }
}