﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RoadPolice.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace RoadPolice.Controllers
{
    [Authorize(Roles = "admin")]
    public class PoliceInspectorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PoliceInspectors
        public ActionResult Index()
        {
            return View(db.PoliceInspectors.ToList());
        }

        // GET: PoliceInspectors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceInspector policeInspector = db.PoliceInspectors.Find(id);
            if (policeInspector == null)
            {
                return HttpNotFound();
            }
            return View(policeInspector);
        }

        // GET: PoliceInspectors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PoliceInspectors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FullName,Rank,Email,Password")] PoliceInspector policeInspector)
        {
            if (ModelState.IsValid)
            {
                db.PoliceInspectors.Add(policeInspector);
                db.SaveChanges();

                var context = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = new ApplicationUser { UserName = policeInspector.Email, Email = policeInspector.Email };
                var result = userManager.Create(user, policeInspector.Password);

                if (result.Succeeded)
                {
                    userManager.AddToRole(user.Id, "inspector");
                }
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(policeInspector);
        }

        // GET: PoliceInspectors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceInspector policeInspector = db.PoliceInspectors.Find(id);
            if (policeInspector == null)
            {
                return HttpNotFound();
            }
            return View(policeInspector);
        }

        // POST: PoliceInspectors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FullName,Rank")] PoliceInspector policeInspector)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policeInspector).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(policeInspector);
        }

        // GET: PoliceInspectors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PoliceInspector policeInspector = db.PoliceInspectors.Find(id);
            if (policeInspector == null)
            {
                return HttpNotFound();
            }
            return View(policeInspector);
        }

        // POST: PoliceInspectors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PoliceInspector policeInspector = db.PoliceInspectors.Find(id);
            db.PoliceInspectors.Remove(policeInspector);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
